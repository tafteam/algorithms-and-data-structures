package com.gaurav.algos;
import com.gaurav.algos.LLQueue;
public class MinMaxHeap {
	private Node top,last,deepest_node;
	LLQueue Q= new LLQueue();
	MinMaxHeap(){
		top=null;
	}
	class Node{
		String data;int priority;
		Node left,right,parent;
		Node(String data,int priority){
			this.data=data;
			this.priority=priority;
			this.left=null;
			this.right=null;
			this.parent=null;
		}
	}
	
	public void perculateDown(Node tmp){
		System.out.println("Inside Perculate Down");
		System.out.println("TOP="+tmp.data);
		int left_priority,right_priority;
		String left_data,right_data;
		if (tmp.left==null) {
			left_priority=-1;
			left_data=null;
			System.out.println("Skipping left as this is null");
		}else{
			left_priority=tmp.left.priority;
			left_data=tmp.left.data;
			System.out.println("Left data to check with="+left_data);
		}
		if (tmp.right==null){
			right_priority=-1;
			right_data=null;
			System.out.println("Skipping right as this is null");
		}else{
			right_priority=tmp.right.priority;
			right_data=tmp.right.data;
			System.out.println("Right data to check with="+right_data);
		}
		if ( (left_priority >=right_priority)&&(left_priority>=tmp.priority) && (left_priority!=-1)){
			System.out.println("Swap left"+tmp.data +" and "+left_data);
			String swap_data=tmp.data;
			tmp.data=left_data;
			tmp.left.data=swap_data;
			int swap_priority=tmp.priority;
			tmp.priority=tmp.left.priority;
			tmp.left.priority=swap_priority;
			tmp=tmp.left;
			System.out.println("tmp left="+tmp.left.data);
		}
		else
			if ((right_priority >= left_priority) &&(right_priority>=tmp.priority)&&(right_priority!=-1)){
				System.out.println("Swap right"+tmp.data +" and "+right_data);
				String swap_data=tmp.data;
				tmp.data=right_data;
				tmp.right.data=swap_data;
				int swap_priority=tmp.priority;
				tmp.priority=tmp.right.priority;
				tmp.right.priority=swap_priority;
				tmp=tmp.right;
				System.out.println("tmp right="+tmp.right.data);
			}
		else{
				System.out.println("Nothing to swap down!! exit");
				if (deepest_node.parent.left!=null) {
					deepest_node=deepest_node.parent.left;
					System.out.println("Setting after pop deepest in left to="+deepest_node.data);
				}
				else 
					if (deepest_node.parent.left!=null){
					deepest_node=deepest_node.parent.right;
					System.out.println("Setting after pop deepest in right to="+deepest_node.data);
					}
				
				else {
					System.out.println("Not sure about this one");
					Node node=(Node)Q.LLPeek();
					deepest_node=node;
					System.out.println("Setting after pop deepest to="+deepest_node.data);
				}
				return;
		}
		perculateDown(tmp);
	}	
	
	public void add(String data,int priority){
		Node node = new Node(data,priority);
		deepest_node=node;
		if (top==null){
			System.out.println("Adding top="+node.data+" with priority "+node.priority);
			top=node;
			last=node;
			Q.LLEnque(last);
			return;
		}
		
		boolean found=false;
		System.out.println("Adding top="+node.data+" with priority "+node.priority);
		while (!Q.isEmpty()&&!found){
			
			last=(Node)Q.LLPeek();
			//System.out.println("Last="+last.data);
			if (last.left==null) {
				System.out.println("Adding "+node.data+" to left of="+last.data);
				last.left=node;
				node.parent=last;
				found=true;
				//System.out.println("Last while enque="+last.data);
				//Q.LLEnque(last);
				Q.LLEnque(last.left);
				//Q.printQueue();
				break;
			}
			else if(last.right == null){
				System.out.println("Adding "+node.data+" to right of="+last.data);
				last.right=node;
				node.parent=last;
				found=true;
				//System.out.println("Last while enque="+last.data);
				Q.LLDequeue();
				Q.LLEnque(last.right);
				//Q.printQueue();
				break;
			}
		}
		perculateUp(node);
					
	}
	public Node getLast(){
		return (Node)Q.LLPeek();
	}
	public void perculateUp(Node node){
		System.out.println("Inside Perculate");
		int left_priority,right_priority,parent_priority;
		if (node.parent==null) {
			System.out.println("This is the parent node!! exit");
			System.out.println("Deepest Node from root="+deepest_node.data+"priority="+deepest_node.priority);
			parent_priority=0;
			return;
		}
		else{
			parent_priority=node.parent.priority;
			String parent_data=node.parent.data;
		}
		
		boolean skip_left= false;
		boolean skip_right = false;
		
		if (node.parent== null) {
			left_priority=-1;
			skip_left = true;
		}
		else  {
			left_priority=node.parent.left.priority;
			String left_data= node.parent.left.data;
		}
		
		if (node.parent.right== null) {
			right_priority=-1;
			skip_right = true;
		}
		else
		  right_priority=node.parent.right.priority;
		
		if ( (left_priority >= right_priority) && (left_priority>=parent_priority) && (left_priority!=-1) ){
			System.out.println("Swap data of "+node.parent.data+" and "+node.parent.left.data);
			node.parent.priority=left_priority;
			node.priority=parent_priority;
			String swap_data=node.data;
			node.data=node.parent.data;
			node.parent.data=swap_data;
			
			node=node.parent;
		}
		
		
		else if ((right_priority >= left_priority) && (right_priority>=parent_priority)  &&(right_priority!=-1)){
			System.out.println("Swap data of "+node.parent.data+" and "+node.parent.right.data);
			node.parent.priority=right_priority;
			node.priority=parent_priority;
			String swap_data=node.parent.data;
			node.parent.data=node.parent.right.data;
			node.data=swap_data;
			node=node.parent;
		
		}
		else{
			System.out.println("No more swapping!!!");
			System.out.println("Deepest node="+deepest_node.data);
			return;
		}
		perculateUp(node);
	}
		
	
	
	public String getMax(){
		String max=top.data;
		//last=getLast();
		top.data=deepest_node.data;
		top.priority=deepest_node.priority;
		deepest_node=null;
		perculateDown(top);
		//System.out.println(max);
		return max;
	}
	
	public static void main(String [] args){
		MinMaxHeap heap = new MinMaxHeap();
		heap.add("A",1);
		heap.add("B",2);
		heap.add("C",3);
		heap.add("D",4);
		heap.add("E",5);
		heap.add("F",6);
		heap.add("G",7);
		heap.add("H",8);
		heap.add("I",9);
		System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		//System.out.println("Max="+heap.getMax());
		
	}

}
