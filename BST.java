package com.gaurav.algos;

import com.gaurav.algos.LLQueue;
public class BST {
	/**
	 * This implementation creates a tree and traverses
	 * 1. Depth First Traversal
	 * 	- Preorder
	 *  - Inorder
	 *  - PostOrder
	 * 2. Breadth First Traversal
	 * TBD:
	 * Delete Node
	 */
	private  Node root;
		
	BST(){
		root=null;
	}

	public class Node{
		int data;
		Node left,right;
		Node(int data){
			this.data=data;
			this.left=null;
			this.right=null;
		}
	
	}
			
	public void add(int data){
		Node node= new Node(data);
		Node tmp=root;
		if (root==null){
			System.out.println("First Node");
			root=node;
			return;
		}
		
		while(tmp!=null){
			if (tmp.data>data){
				if (tmp.left==null) {
					System.out.println("Adding Node to the left of="+tmp.data);
					tmp.left=node;
					return;
				}
				else tmp=tmp.left;
			}
			else{
				if (tmp.right==null){
					System.out.println("Adding Node to the right of="+tmp.data);
					tmp.right=node;
					return;
				}
				else tmp=tmp.right;
				
			}
		}
		
		
	}
	
	public void preorderTraversal(){
		Node tmp=root;
		preOrderTraverseNode(tmp);
	}
	
	
	private void preOrderTraverseNode(Node node) {
		// TODO Auto-generated method stub
		if (node == null) return;
		System.out.println(node.data);
		preOrderTraverseNode(node.left);
		preOrderTraverseNode(node.right);
	}
	
	public void inOrderTraversal(){
		Node tmp=root;
		inOrderTraverseNode(tmp);
	}
	
	private void inOrderTraverseNode(Node node){
		if (node==null) return;
		inOrderTraverseNode(node.left);
		System.out.println(node.data);
		inOrderTraverseNode(node.right);
	}
	
	private void levelOrderTraversal(){
		LLQueue Q = new LLQueue();
		Node tmp=root;
		if (root==null) return;
		Q.LLEnque(root);
		while (!Q.isEmpty()){
			tmp=(BST.Node)Q.LLDequeue();
			System.out.print(tmp.data+"-->");
			if (tmp.left!=null) Q.LLEnque(tmp.left);
			if (tmp.right!=null) Q.LLEnque(tmp.right);
		}
	}

	public static void main(String [] args){
		BST tree= new BST();
		tree.add(5);
		tree.add(6);
		tree.add(7);
		tree.add(4);
		tree.add(3);
		tree.add(1);
		//tree.add(2);
		//tree.preorderTraversal();
		tree.inOrderTraversal();
		//tree.levelOrderTraversal();
		//tree.postOrderTraversal(root);
	}

}
