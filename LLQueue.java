package com.gaurav.algos;

public class LLQueue {
	private Node head,tail;
	
	private class Node{
		private Object data;
		private Node next;
		
		Node(Object data){
			this.data=data;
			this.next=null;
		}
		
	}
	
	LLQueue(){
		head=null;
		tail=null;
	}
	
	public void LLEnque(Object data){
		Node node= new Node(data);
		if (head==null){
			//System.out.println("First Node="+((BST.Node)node.data).data);
			head=node;
			tail=head;
			return;
		}
		Node tmp=head;
		while(tmp.next!=null){
			tmp=tmp.next;
		}
		tmp.next=node;
	}
	
	public Object LLDequeue(){
		
		Node tmp=head;
		head= head.next;
		//System.out.println("Dequeing="+((BST.Node)tmp.data).data);
		return tmp.data;
	}
	public Object LLPeek(){
		Node tmp=head;
		return tmp.data;
	}
	public boolean isEmpty(){
		if (head==null) 
			return true;
		return false;
	}
	
	public void printQueue(){
		Node tmp = head;
		while(tmp!=null){
			//System.out.print(tmp.data+"-->");
			tmp=tmp.next;
		}
	}
	
	public static void main (String [] args){
		LLQueue queue= new LLQueue();
		queue.LLEnque(5);
		queue.LLEnque(6);
		queue.LLEnque(7);
		queue.LLEnque(3);
		queue.LLEnque(2);
		queue.LLEnque(1);
		queue.LLEnque(0);
		queue.printQueue();
		queue.LLDequeue();
		queue.LLDequeue();
		queue.LLDequeue();
		queue.LLDequeue();
		queue.printQueue();
	}
}
